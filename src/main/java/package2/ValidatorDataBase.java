package package2;

import java.util.Scanner;

public class ValidatorDataBase {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Decide want you want to do:");
		System.out.println("1 - validate name");
		System.out.println("2 - validate surname");
		System.out.println("3 - validate date of birth format dd-mm-yyyy");
		System.out.println("4 - quit");
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (line.equals("1")) {
				System.out.println("Give name!");
				String lineName = sc.next();
				validateNameAndSurname(lineName);
				continue;
			} else if (line.equals("2")) {
				System.out.println("Give surname!");
				String lineSurname = sc.next();
				validateNameAndSurname(lineSurname);
				continue;
			} else if (line.equals("3")) {
				System.out.println("Give date of birth!");
				String lineDateOfBirth = sc.next();
				validateDateOfBirth(lineDateOfBirth);
				continue;
			}

			if (line.equals("4")) {
				System.out.println("Quit!");
				break;
			}

		}

	}

	private static void validateDateOfBirth(String line) {
		//ADD VALIDATE DELIMITER
		
		String[] partOfDateOfBirth = line.split("-");
		String parts1 = partOfDateOfBirth[0];
		String parts2 = partOfDateOfBirth[1];
		String parts3 = partOfDateOfBirth[2];
		if (parts1.matches("[0-9]+") && parts2.matches("[0-9]+") && parts3.matches("[0-9]+") && parts1.length() == 2
				&& parts2.length() == 2 && parts3.length() == 4) {
			System.out.println("Correct");
		} else {
			System.out.println("Please give a correct date");
		}

	}

	private static void validateNameAndSurname(String line) {
		if (!line.matches("[a-zA-Z]+") || line.equals("")) {
			System.out.println("Please give a correct signs");

		} else {
			System.out.println("Correct");
		}
	}
}
